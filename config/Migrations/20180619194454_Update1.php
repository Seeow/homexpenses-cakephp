<?php
use Migrations\AbstractMigration;

class Update1 extends AbstractMigration
{

    public function up()
    {

        $this->table('users')
            ->removeColumn('firstname')
            ->removeColumn('lastname')
            ->update();

        $this->table('users')
            ->addColumn('name', 'string', [
                'after' => 'password',
                'default' => null,
                'length' => 255,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('users')
            ->addColumn('firstname', 'string', [
                'after' => 'password',
                'default' => null,
                'length' => 255,
                'null' => true,
            ])
            ->addColumn('lastname', 'string', [
                'after' => 'firstname',
                'default' => null,
                'length' => 255,
                'null' => true,
            ])
            ->removeColumn('name')
            ->update();
    }
}

