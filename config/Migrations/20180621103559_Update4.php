<?php
use Migrations\AbstractMigration;

class Update4 extends AbstractMigration
{

    public function up()
    {

        $this->table('expense_users')
            ->changeColumn('percentage', 'float', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('expense_users')
            ->changeColumn('percentage', 'integer', [
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->update();
    }
}

