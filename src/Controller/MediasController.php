<?php

namespace App\Controller;

use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Utility\Security;

class MediasController extends AppController
{
    public function upload()
    {
        $defaultError = __("Une erreur est survenue");
        $response = [
            'error' => false,
            'message' => ""
        ];

        $file = $this->request->getData('file');
        $ref = $this->request->getData('ref');
        $refId = $this->request->getData('refId');
        $refField = $this->request->getData('refField');
        $crop = $this->request->getData('crop');

        if ($file && $ref && $refId) {
            $Table = TableRegistry::get($ref);
            if ($Table->behaviors()->has('Media') && isset($Table->behaviors()->get('Media')->getConfig('fields')[$refField])) {
                $validExtensions = $Table->behaviors()->get('Media')->getConfig('fields')[$refField]['extensions'];
                $private = $Table->behaviors()->get('Media')->getConfig('fields')[$refField]['private'];
                if ($private) {
                    $randomBytes = 80;
                } else {
                    $randomBytes = 8;
                }
                $infoFile = pathinfo($file['name']);
                $fileName = bin2hex(Security::randomBytes($randomBytes));
                $fileExtension = strtolower($infoFile['extension']);
                if (!empty($validExtensions) && in_array($fileExtension, $validExtensions)) {
                    if ($crop == 'true') {
                        $filePath = WWW_ROOT . 'uploads' . DS . 'tmp';
                    } else {
                        $filePath = WWW_ROOT . 'uploads' . DS . $ref . DS . $refId;
                    }
                    $newFile = $filePath . DS . $fileName . '.' . $fileExtension;

                    if (!file_exists($filePath)) {
                        $folder = new Folder();
                        $folder->create($filePath);
                    }

                    $folder = new Folder($filePath);
                    $files = $folder->read(true, false, true);
                    foreach ($files[1] as $val) {
                        $f = new File($val);
                        $lastChange = $f->lastChange();
                        if ($lastChange) {
                            $lastChange = new Time($lastChange);
                            if (!$lastChange->wasWithinLast('48 hours')) {
                                $f->delete();
                            }
                        }
                    }

                    if ($crop == 'false') {
                        $entity = $Table->get($refId);
                        if ($entity) {
                            $oldValue = $entity->$refField;
                            $entity->$refField = $fileName . '.' . $fileExtension;

                            if ($Table->save($entity)) {
                                if ($oldValue) {
                                    $oldFile = new File($filePath . DS . $oldValue);
                                    if ($oldFile->exists()) {
                                        $oldFile->delete();
                                    }
                                }
                            } else {
                                $response['error'] = true;
                                $response['message'] = $defaultError;
                            }
                        } else {
                            $response['error'] = true;
                            $response['message'] = $defaultError;
                        }
                    }

                    if ($response['error'] == false) {
                        if (move_uploaded_file($file['tmp_name'], $newFile)) {
                            if (file_exists($newFile)) {
                                $info = getimagesize($newFile);

                                if ($info[2] === IMAGETYPE_JPEG) {
                                    $exif = @exif_read_data($newFile);
                                    if (!empty($exif['Orientation'])) {
                                        $image = @imagecreatefromjpeg($newFile);
                                        if (!$image) {
                                            $image = imagecreatefromstring(file_get_contents($newFile));
                                        }

                                        if (isset($image)) {
                                            switch ($exif['Orientation']) {
                                                case 8:
                                                    $image = imagerotate($image, 90, 0);
                                                    break;

                                                case 3:
                                                    $image = imagerotate($image, 180, 0);
                                                    break;

                                                case 6:
                                                    $image = imagerotate($image, -90, 0);
                                                    break;
                                            }

                                            imagejpeg($image, $newFile, 100);
                                        }
                                    }

                                    /*if (filesize($newFile) > 2000000) {
                                        $image = imagecreatefromjpeg($newFile);
                                        imagejpeg($image, $newFile, 50);
                                    }*/
                                }

                                $response['image'] = [
                                    'name' => $fileName . '.' . $fileExtension,
                                    'extension' => $fileExtension
                                ];
                                if ($crop == 'true') {
                                    $response['image']['src'] = Router::url('/uploads/tmp/' . $fileName . '.' . $fileExtension, true);
                                } else {
                                    $response['image']['src'] = Router::url('/uploads/' . $ref . '/' . $refId . '/' . $fileName . '.' . $fileExtension, true);
                                }
                            }
                        } else {
                            $response['error'] = true;
                            $response['message'] = $defaultError;
                        }
                    }
                } else {
                    $response['error'] = true;
                    $message = __("Le fichier doit être au format");
                    foreach ($validExtensions as $validExtension) {
                        $message .= ' ' . $validExtension . ',';
                    }
                    $response['message'] = trim($message, ',');
                }
            } else {
                $response['error'] = true;
                $response['message'] = $defaultError;
            }
        } else {
            $response['error'] = true;
            $response['message'] = $defaultError;
        }

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function crop()
    {
        $defaultError = __("Une erreur est survenue");
        $response = [
            'error' => false,
            'message' => ""
        ];

        $fileName = $this->request->getData('fileName');
        $ref = $this->request->getData('ref');
        $refId = $this->request->getData('refId');
        $refField = $this->request->getData('refField');
        $filePath = WWW_ROOT . 'uploads' . DS . 'tmp';
        $file = $filePath . DS . $fileName;
        $newFilePath = WWW_ROOT . 'uploads' . DS . $ref . DS . $refId;
        $newFile = $newFilePath . DS . $fileName;

        if (!file_exists($newFilePath)) {
            $folder = new Folder();
            $folder->create($newFilePath);
        }

        if (file_exists($file)) {
            $info = getimagesize($file);
            list($widthOld, $heightOld) = $info;

            switch ($info[2]) {
                case IMAGETYPE_GIF:
                    $image = imagecreatefromgif($file);
                    break;

                case IMAGETYPE_JPEG:
                    $image = imagecreatefromjpeg($file);
                    break;

                case IMAGETYPE_PNG:
                    $image = imagecreatefrompng($file);
                    break;
            }

            if (isset($image)) {
                $newImage = imagecreatetruecolor($this->request->getData('ratioX'), $this->request->getData('ratioY'));

                $transparencyIndex = imagecolortransparent($image);
                $transparencyColor = ['red' => 255, 'green' => 255, 'blue' => 255];
                if ($transparencyIndex >= 0) {
                    $transparencyColor = imagecolorsforindex($image, $transparencyIndex);
                }
                $transparencyIndex = imagecolorallocate($newImage, $transparencyColor['red'], $transparencyColor['green'], $transparencyColor['blue']);
                imagefill($newImage, 0, 0, $transparencyIndex);
                imagecolortransparent($newImage, $transparencyIndex);
                imagealphablending($newImage, false);
                imagesavealpha($newImage, true);

                imagecopyresampled($newImage, $image, 0, 0, $this->request->getData('x'), $this->request->getData('y'), $this->request->getData('ratioX'), $this->request->getData('ratioY'), $this->request->getData('width'), $this->request->getData('height'));

                $Table = TableRegistry::get($ref);
                $entity = $Table->get($refId);
                if ($entity) {
                    $oldValue = $entity->$refField;
                    $entity->$refField = $fileName;

                    if ($Table->save($entity)) {
                        switch ($info[2]) {
                            case IMAGETYPE_JPEG:
                                imagejpeg($newImage, $newFile, 100);
                                break;

                            case IMAGETYPE_PNG:
                                imagepng($newImage, $newFile, 9);
                                break;
                        }

                        if ($oldValue) {
                            $oldFile = new File($newFilePath . DS . $oldValue);
                            if ($oldFile->exists()) {
                                $oldFile->delete();
                            }
                        }

                        $response['image'] = [
                            'name' => $fileName,
                            'src' => Router::url('/uploads/' . $ref . '/' . $refId . '/' . $fileName, true)
                        ];
                    } else {
                        $response['error'] = true;
                        $response['message'] = $defaultError;
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = $defaultError;
                }
            } else {
                $response['error'] = true;
                $response['message'] = $defaultError;
            }
        } else {
            $response['error'] = true;
            $response['message'] = $defaultError;
        }

        $f = new File($file);
        if ($f->exists()) {
            $f->delete();
        }

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }
}
