<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;

class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['validateToken', 'login', 'register']);
    }

    public function index()
    {
        $response = [
            'error' => false,
            'message' => null
        ];

        $users = $this->Users->find()
            ->order(['Users.name' => 'ASC']);

        $users->each(function ($value, $key) {
            $value->virtualProperties(['amount', 'amount_display']);
        });

        $response['users'] = $users;

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function add()
    {
        $response = [
            'error' => true,
            'message' => __("Une erreur est survenue")
        ];

        $data = [
            'email' => $this->request->getData('email'),
            'name' => $this->request->getData('name'),
            'mode' => 'create'
        ];

        $user = $this->Users->newEntity($data, [
            'fieldList' => ['email', 'name']
        ]);
        $user->active = true;
        $user->validated = false;
        $user->validate_token = bin2hex(Security::randomBytes(64));

        if ($this->Users->save($user)) {
            $email = new Email();
            $email->setTemplate('users/new')
                ->setEmailFormat('html')
                ->addTo($user->email)
                ->setSubject(__("Invitation à utiliser {0}", Configure::read('Settings.name')))
                ->setViewVars([
                    'appUrl' => Configure::read('Settings.appUrl'),
                    'user' => $user
                ])
                ->send();

            $response['error'] = false;
            $response['message'] = __("{0} sauvegardé", $user->name);
        } else {
            if (!empty($user->errors())) {
                $response['message'] = null;
                $response['errors'] = [];
                foreach ($user->errors() as $field => $errors) {
                    foreach ($errors as $error) {
                        $response['errors'][$field] = $error;
                    }
                }
            }
        }

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function edit($id)
    {
        $response = [
            'error' => true,
            'message' => __("Une erreur est survenue")
        ];

        $user = $this->Users->find()
            ->where([
                'Users.id' => $id
            ])
            ->first();

        if ($user) {
            $user = $this->Users->patchEntity($user, $this->request->getData(), [
                'fieldList' => ['email', 'name']
            ]);

            if ($this->Users->save($user)) {
                $response['error'] = false;
                $response['message'] = __("{0} sauvegardé", $user->name);
            } else {
                if (!empty($user->errors())) {
                    $response['message'] = null;
                    $response['errors'] = [];
                    foreach ($user->errors() as $field => $errors) {
                        foreach ($errors as $error) {
                            $response['errors'][$field] = $error;
                        }
                    }
                }
            }
        }

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function me()
    {
        $response = [
            'error' => false,
            'message' => null
        ];

        $user = $this->Auth->user();
        unset($user['access_token']);
        $response['user'] = $user;

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function editMe()
    {
        $response = [
            'error' => true,
            'message' => __("Une erreur est survenue")
        ];

        $user = $this->Users->find()
            ->where([
                'Users.id' => $this->Auth->user('id')
            ])
            ->first();

        if ($user) {
            $data = $this->request->getData();

            $user = $this->Users->patchEntity($user, $data, [
                'fieldList' => ['email', 'name']
            ]);

            if ($this->Users->save($user, ['access_token' => $this->Auth->user('access_token')])) {
                $response['user'] = $user;
                $response['error'] = false;
                $response['message'] = __("Profil sauvegardé");
            } else {
                if (!empty($user->errors())) {
                    $response['message'] = null;
                    $response['errors'] = [];
                    foreach ($user->errors() as $field => $errors) {
                        foreach ($errors as $error) {
                            $response['errors'][$field] = $error;
                        }
                    }
                }
            }
        }

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function editPassword()
    {
        $response = [
            'error' => true,
            'message' => __("Une erreur est survenue")
        ];

        $user = $this->Users->find()
            ->where([
                'Users.id' => $this->Auth->user('id')
            ])
            ->first();

        if ($user) {
            $data = $this->request->getData();
            $data['mode'] = 'password';

            $user = $this->Users->patchEntity($user, $data, [
                'fieldList' => ['password', 'password2']
            ]);

            if ($this->Users->save($user, ['access_token' => $this->Auth->user('access_token')])) {
                $response['user'] = $user;
                $response['error'] = false;
                $response['message'] = __("Profil sauvegardé");
            } else {
                if (!empty($user->errors())) {
                    $response['message'] = null;
                    $response['errors'] = [];
                    foreach ($user->errors() as $field => $errors) {
                        foreach ($errors as $error) {
                            $response['errors'][$field] = $error;
                        }
                    }
                }
            }
        }

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function validateToken()
    {
        $response = [
            'error' => true,
            'message' => __("Une erreur est survenue")
        ];

        $user = $this->Users->find()
            ->where([
                'Users.validate_token' => $this->request->getQuery('token'),
                'Users.validated' => false
            ])
            ->first();

        if ($user) {
            $response['user'] = $user;
            $response['error'] = false;
            $response['message'] = null;
        }

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function register()
    {
        $response = [
            'error' => true,
            'message' => __("Une erreur est survenue")
        ];

        $user = $this->Users->find()
            ->where([
                'Users.email' => $this->request->getData('email'),
                'Users.validate_token' => $this->request->getData('token'),
                'Users.validated' => false
            ])
            ->first();

        if ($user) {
            $data = [
                'email' => $this->request->getData('email'),
                'name' => $this->request->getData('name'),
                'password' => $this->request->getData('password'),
                'password2' => $this->request->getData('password2'),
                'mode' => 'register'
            ];
            $user = $this->Users->patchEntity($user, $data, [
                'fieldList' => ['email', 'name', 'password', 'password2']
            ]);
            $user->validated = true;
            $user->validate_token = null;

            if ($this->Users->save($user)) {
                $accessToken = TableRegistry::getTableLocator()->get('AccessTokens')->generate($user->id);

                $response['access_token'] = $accessToken->token;
                $response['user'] = $user;
                $response['error'] = false;
                $response['message'] = __("Bonjour {0}", $user->name);
            } else {
                if (!empty($user->errors())) {
                    $response['message'] = null;
                    $response['errors'] = [];
                    foreach ($user->errors() as $field => $errors) {
                        foreach ($errors as $error) {
                            $response['errors'][$field] = $error;
                        }
                    }
                }
            }
        }

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function login()
    {
        $response = [
            'error' => true,
            'message' => __("Identifiants incorrects")
        ];

        $user = $this->Auth->identify();
        if ($user) {
            $accessToken = TableRegistry::getTableLocator()->get('AccessTokens')->generate($user['id']);

            $response['access_token'] = $accessToken->token;
            $response['user'] = $user;
            $response['error'] = false;
            $response['message'] = __("Bonjour {0}", $user['name']);
        }

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function logout()
    {
        $response = [
            'error' => false,
            'message' => null
        ];

        $AccessTokens = TableRegistry::get('AccessTokens');
        $accessToken = $AccessTokens->find()
            ->where([
                'AccessTokens.token' => $this->Auth->user('access_token'),
                'AccessTokens.user_id' => $this->Auth->user('id')
            ])
            ->first();

        if ($accessToken) {
            $AccessTokens->delete($accessToken);
        }

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function delete($id)
    {
        $response = [
            'error' => true,
            'message' => __("Une erreur est survenue")
        ];

        $user = $this->Users->find()
            ->where([
                'Users.id' => $id
            ])
            ->first();

        if ($user) {
            if ($this->Users->delete($user)) {
                $response['error'] = false;
                $response['message'] = __("{0} supprimé", $user->name);
            }
        }

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }
}
