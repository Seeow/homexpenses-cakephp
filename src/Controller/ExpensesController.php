<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Mailer\Email;
use Cake\Utility\Security;

class ExpensesController extends AppController
{
    public function index()
    {
        $response = [
            'error' => false,
            'message' => null
        ];

        $response['expenses'] = $this->Expenses->find()
            ->contain([
                'Payer',
                'Users'
            ])
            ->order(['Expenses.date' => 'DESC']);

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function add()
    {
        $response = [
            'error' => true,
            'message' => __("Une erreur est survenue")
        ];

        $data = [
            'email' => $this->request->getData('email'),
            'name' => $this->request->getData('name'),
            'mode' => 'create'
        ];

        $user = $this->Users->newEntity($data, [
            'fieldList' => ['email', 'name']
        ]);
        $user->active = true;
        $user->validated = false;
        $user->validate_token = bin2hex(Security::randomBytes(64));

        if ($this->Users->save($user)) {
            $email = new Email();
            $email->setTemplate('users/new')
                ->setEmailFormat('html')
                ->addTo($user->email)
                ->setSubject(__("Invitation à utiliser {0}", Configure::read('Settings.name')))
                ->setViewVars([
                    'appUrl' => Configure::read('Settings.appUrl'),
                    'user' => $user
                ])
                ->send();

            $response['error'] = false;
            $response['message'] = __("{0} sauvegardé", $user->name);
        } else {
            if (!empty($user->errors())) {
                $response['message'] = null;
                $response['errors'] = [];
                foreach ($user->errors() as $field => $errors) {
                    foreach ($errors as $error) {
                        $response['errors'][$field] = $error;
                    }
                }
            }
        }

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function edit($id = null)
    {
        $response = [
            'error' => true,
            'message' => __("Une erreur est survenue")
        ];

        if ($id) {
            $expense = $this->Expenses->find()
                ->where([
                    'Expenses.id' => $id
                ])
                ->first();
        } else {
            $expense = $this->Expenses->newEntity();
        }

        if ($expense) {
            $data = [
                'name' => $this->request->getData('name'),
                'amount' => $this->request->getData('amount'),
                'date' => (new Time($this->request->getData('date_formated')))->format('Y-m-d'),
                'user_id' => $this->request->getData('user_id'),
                'users' => []
            ];
            if ($this->request->getData('users')) {
                foreach ($this->request->getData('users') as $user) {
                    if (!empty($user['_joinData']['percentage']) && $user['_joinData']['percentage'] > 0) {
                        $data['users'][] = $user;
                    }
                }
            }

            $expense = $this->Expenses->patchEntity($expense, $data, [
                'associated' => [
                    'Users' => [
                        'accessibleFields' => ['id' => true]
                    ]
                ]
            ]);

            if ($this->Expenses->save($expense)) {
                $response['error'] = false;
                $response['message'] = __("{0} sauvegardé", $expense->name);
            } else {
                if (!empty($expense->errors())) {
                    $response['message'] = null;
                    $response['errors'] = [];
                    foreach ($expense->errors() as $field => $errors) {
                        foreach ($errors as $error) {
                            $response['errors'][$field] = $error;
                        }
                    }
                }
            }
        }

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function delete($id)
    {
        $response = [
            'error' => true,
            'message' => __("Une erreur est survenue")
        ];

        $expense = $this->Expenses->find()
            ->where([
                'Expenses.id' => $id
            ])
            ->first();

        if ($expense) {
            if ($this->Expenses->delete($expense)) {
                $response['error'] = false;
                $response['message'] = __("{0} supprimé", $expense->name);
            }
        }

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }
}
