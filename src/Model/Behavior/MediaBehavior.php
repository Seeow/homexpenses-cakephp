<?php

namespace App\Model\Behavior;

use Cake\Filesystem\Folder;
use Cake\ORM\Behavior;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;

class MediaBehavior extends Behavior
{
    public function initialize(array $config)
    {
        foreach ($config as $field => $options) {
            if (!isset($options['private'])) {
                $config[$field]['private'] = false;
            }
        }
        $this->setConfig('fields', $config);
    }

    public function beforeDelete(Event $event, EntityInterface $entity)
    {
        $f = new Folder(WWW_ROOT . 'uploads' . DS . $this->getTable()->getAlias() . DS . $entity->id);
        if (!empty($f->path) && !$f->delete()) {
            $event->stopPropagation();
        }
    }
}
