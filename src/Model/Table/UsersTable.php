<?php

namespace App\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Timestamp');
        $this->addBehavior('Media', [
            'avatar' => [
                'extensions' => ['jpg', 'jpeg', 'png']
            ]
        ]);

        $this->hasMany('AccessTokens', [
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('MyExpenses', [
            'className' => 'Expenses',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->belongsToMany('Expenses', [
            'joinTable' => 'expense_users',
            'through' => 'ExpenseUsers'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence([
                'email' => [
                    'mode' => 'create',
                    'message' => "L'email est incorrect"
                ],
                'name' => [
                    'mode' => 'create',
                    'message' => "Le nom est incorrect"
                ]
            ])
            ->requirePresence([
                'email' => [
                    'mode' => function ($context) {
                        return $this->_requirePresenceRegister($context);
                    },
                    'message' => "L'email est incorrect"
                ],
                'name' => [
                    'mode' => function ($context) {
                        return $this->_requirePresenceRegister($context);
                    },
                    'message' => "Le nom est incorrect"
                ],
                'password' => [
                    'mode' => function ($context) {
                        return $this->_requirePresenceRegister($context);
                    },
                    'message' => "Le mot de passe est incorrect"
                ],
                'password2' => [
                    'mode' => function ($context) {
                        return $this->_requirePresenceRegister($context);
                    },
                    'message' => "Ce champ ne peut pas être vide"
                ]
            ])
            ->requirePresence([
                'password' => [
                    'mode' => function ($context) {
                        if (isset($context['data']['mode']) && $context['data']['mode'] === 'password') {
                            return true;
                        }

                        return false;
                    },
                    'message' => "Ce champ ne peut pas être vide"
                ],
                'password2' => [
                    'mode' => function ($context) {
                        if (isset($context['data']['mode']) && $context['data']['mode'] === 'password') {
                            return isset($context['data']['password']);
                        }

                        return false;
                    },
                    'message' => "Ce champ ne peut pas être vide"
                ]
            ])
            ->notEmpty('email', "L'email est incorrect")
            ->notEmpty('password', "Le mot de passe est incorrect")
            ->notEmpty('password2', "Ce champ ne peut pas être vide")
            ->notEmpty('name', "Le nom est incorrect")
            ->add('email', [
                'email' => [
                    'rule' => ['email'],
                    'message' => "L'email est incorrect"
                ]
            ])
            ->add('password2', [
                'compare' => [
                    'rule' => ['compareWith', 'password'],
                    'message' => "Le mot de passe ne correspond pas"
                ]
            ]);

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email'], "L'email existe déjà"));

        return $rules;
    }

    public function findAuth(Query $query, array $options)
    {
        $query
            ->where([
                'Users.active' => true,
                'Users.validated' => true
            ]);

        return $query;
    }

    protected function _requirePresenceRegister($context)
    {
        if (isset($context['data']['mode'])) {
            return $context['data']['mode'] === 'register';
        }

        return false;
    }

    public function afterSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        if ($entity->isDirty('email') || $entity->isDirty('password')) {
            $conditions = [
                'AccessTokens.user_id' => $entity->id
            ];

            if (!empty($options['access_token'])) {
                $conditions['NOT']['AccessTokens.token'] = $options['access_token'];
            }

            TableRegistry::get('AccessTokens')->deleteAll($conditions);
        }
    }
}
