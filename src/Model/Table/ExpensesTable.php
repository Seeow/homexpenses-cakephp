<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ExpensesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo('Payer', [
            'className' => 'Users',
            'foreignKey' => 'user_id'
        ]);

        $this->belongsToMany('Users', [
            'joinTable' => 'expense_users',
            'through' => 'ExpenseUsers'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence(['user_id', 'amount', 'name', 'date'])
            ->notEmpty('user_id')
            ->notEmpty('amount')
            ->notEmpty('name')
            ->notEmpty('date');

        return $validator;
    }
}
