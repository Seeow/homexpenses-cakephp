<?php

namespace App\Model\Table;

use Cake\Core\Configure;
use Cake\ORM\Table;
use Cake\Utility\Security;
use Cake\Validation\Validator;
use Firebase\JWT\JWT;

class AccessTokensTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users');
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence(['token', 'user_id', 'user_agent'])
            ->notEmpty('token')
            ->notEmpty('user_id')
            ->notEmpty('user_agent');

        return $validator;
    }

    public function generate($userId = null)
    {
        if (!$userId) {
            return null;
        }

        $iat = time();
        $token = JWT::encode([
            'iat' => $iat,
            'jti' => bin2hex(Security::randomBytes(32)),
            'iss' => env('SERVER_NAME'),
            'nbf' => $iat,
            'exp' => $iat + (Configure::read('Session.timeout') * 60),
            'sub' => $userId
        ], Security::getSalt(), 'HS256');

        $entity = $this->newEntity([
            'token' => $token,
            'user_id' => $userId,
            'user_agent' => $_SERVER['HTTP_USER_AGENT']
        ]);

        if ($this->save($entity)) {
            return $entity;
        }

        return null;
    }
}
