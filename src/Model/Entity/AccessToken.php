<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use DeviceDetector\DeviceDetector;

class AccessToken extends Entity
{
    protected $_accessible = [
        '*' => true
    ];

    protected $_virtual = [
        'name',
        'icon'
    ];

    protected function _getName()
    {
        $userAgent = $this->get('user_agent');
        $name = '';

        $dd = new DeviceDetector($userAgent);
        $dd->discardBotInformation();
        $dd->skipBotDetection();
        $dd->parse();

        if (!$dd->isBot()) {
            $OS = $dd->getOs();
            if ($OS) {
                $name = $OS['name'] . ' ' . $OS['version'];
            }
            $model = $dd->getModel();
            if ($model) {
                $name = $model . ' - ' . $name;
            }
        }

        return $name;
    }

    protected function _getIcon()
    {
        $userAgent = $this->get('user_agent');
        $icon = 'sl sl-icon-screen-desktop';

        $dd = new DeviceDetector($userAgent);
        $dd->discardBotInformation();
        $dd->skipBotDetection();
        $dd->parse();

        if (!$dd->isBot()) {
            $device = $dd->getDeviceName();
            if ($device == 'smartphone') {
                $icon = 'sl sl-icon-screen-smartphone';
            } else if ($device == 'tablet') {
                $icon = 'sl sl-icon-screen-tablet';
            }
        }

        return $icon;
    }
}
