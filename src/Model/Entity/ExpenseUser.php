<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class ExpenseUser extends Entity
{
    protected $_accessible = [
        '*' => true
    ];
}
