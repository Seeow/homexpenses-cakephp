<?php

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

class User extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected $_hidden = [
        'password',
        'validate_token',
        'password_token',
        'password_token_expiration'
    ];

    protected $_virtual = [
        'image'
    ];

    protected $_amount;

    protected function _setPassword($value)
    {
        return (new DefaultPasswordHasher)->hash($value);
    }

    protected function _getAmount()
    {
        if ($this->_amount === null) {
            $id = $this->get('id');
            $expenses = TableRegistry::getTableLocator()->get('Expenses')->find()
                ->contain([
                    'Users' => function ($q) use ($id) {
                        return $q->where([
                            'Users.id' => $id
                        ]);
                    }
                ]);

            $this->_amount = $expenses->sumOf(function ($value) use ($id) {
                $amount = 0;
                if ($value->user_id === $id) {
                    $amount = $value->amount;
                }

                if (!empty($value->users[0])) {
                    $amount -= $value->amount * ($value->users[0]->_joinData->percentage / 100);
                }
                return $amount;
            });
        }
        return $this->_amount;
    }

    protected function _getAmountDisplay()
    {
        $amount = $this->_getAmount();
        return number_format($amount, 2, '.', '\'') . ' CHF';
    }

    protected function _getImage()
    {
        $avatar = $this->get('avatar');
        if ($avatar) {
            return Router::url('/uploads/Users/' . $this->get('id') . '/' . $avatar, true);
        }

        return Router::url('/img/default-avatar.png', true);
    }
}
