<?php

namespace App\Model\Entity;

use Cake\Core\Configure;
use Cake\ORM\Entity;

class Expense extends Entity
{
    protected $_accessible = [
        '*' => true
    ];

    protected $_virtual = [
        'date_formated',
        'date_display',
        'amount_display'
    ];

    protected function _getDateFormated()
    {
        $date = $this->get('date');

        if ($date) {
            return $date->i18nFormat('dd.MM.YYYY', Configure::read('Settings.defaultTimezone'));
        }

        return null;
    }

    protected function _getDateDisplay()
    {
        $date = $this->get('date');

        if ($date) {
            return $date->i18nFormat('dd MMMM YYYY', Configure::read('Settings.defaultTimezone'));
        }

        return null;
    }

    protected function _getAmountDisplay()
    {
        $amount = $this->get('amount');

        if ($amount) {
            return number_format($amount, 2, '.', '\'') . ' CHF';
        }

        return null;
    }
}
